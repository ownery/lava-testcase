#!/bin/bash

set -x


wget --no-check-certificate https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/preview/openEuler-22.03-V2-riscv64/D1/openEuler-22.03-V2-base-d1-preview.img.tar.zst
tar -I zstd -xvf openEuler-22.03-V2-base-d1-preview.img.tar.zst

IODEPTH="10"
SIZE="1G"
NUMJOBS="10"
RUNTIME="30"
FILENAME=$(pwd)/openEuler-22.03-V2-base-d1-preview.img

usage() {
    echo "Usage: ${0} [-i IODEPTH]
                      [-s SIZE]
                      [-n NUMJOBS]
                      [-t RUNTIME]
                      [-f FILENAME]
" 1>&2
    exit 0
}

while getopts "i:s:n:t:f" arg; do
  case "$arg" in
    i)
      IODEPTH="${OPTARG}"
      ;;
    s)
      SIZE="${OPTARG}"
      ;;
    n)
      NUMJOBS="${OPTARG}"
      ;;
    t)
      RUNTIME="${OPTARG}"
      ;;
    f)
      if [ -n "${OPTARG}" ]; then
        FILENAME="${OPTARG}"
      fi
      ;;
    ?)
      usage
      echo "unrecognized argument ${OPTARG}"
      ;;
  esac
done

yum install unzip libaio-devel -y
wget https://github.com/axboe/fio/archive/refs/heads/master.zip
unzip master.zip
cd fio-master
./configure --disable-shm
make && make install

for RW in read write randread randwrite randrw; do
  for BS in 4 16 32 64 128 256 512 1024; do
    if [ ${RW} = "randrw" ]; then
      fio -name=randrw -direct=1 -iodepth=${IODEPTH} -rw=${RW} -rwmixread=70 -ioengine=libaio -bs=${BS}k -size=${SIZE} -numjobs=${NUMJOBS} -runtime=${RUNTIME} -group_reporting -filename=${FILENAME}
    elif [ ${RW} = "randread" ]; then
      fio -name=randread -direct=1 -iodepth=${IODEPTH} -rw=${RW} -ioengine=libaio -bs=${BS}k -size=${SIZE} -numjobs=${NUMJOBS} -runtime=${RUNTIME} -group_reporting -filename=${FILENAME}
    elif [ ${RW} = "randwrite" ]; then
      fio -name=randwrite -direct=1 -iodepth=${IODEPTH} -rw=${RW} -ioengine=libaio -bs=${BS}k -size=${SIZE} -numjobs=${NUMJOBS} -runtime=${RUNTIME} -group_reporting -filename=${FILENAME}
    elif [ ${RW} = "read" ]; then
      fio -name=read -direct=1 -iodepth=${IODEPTH} -rw=${RW} -ioengine=libaio -bs=${BS}k -size=${SIZE} -numjobs=${NUMJOBS} -runtime=${RUNTIME} -group_reporting -filename=${FILENAME}
    elif [ ${RW} = "write" ]; then
      fio -name=write -direct=1 -iodepth=${IODEPTH} -rw=${RW} -ioengine=libaio -bs=${BS}k -size=${SIZE} -numjobs=${NUMJOBS} -runtime=${RUNTIME} -group_reporting -filename=${FILENAME}
    fi
  done
done
