#!/bin/bash

set -x

yum install unzip
wget https://github.com/redhat-performance/libMicro/archive/refs/heads/0.4.0-rh.zip 
unzip 0.4.0-rh.zip
cd libMicro-0.4.0-rh
make
./bench